export default class MonAnV1 {
  constructor(_ma, _ten, _loai, _gia, _khuyenMai, _hinhAnh, _moTa, _tinhTrang) {
    this.ma = _ma;
    this.ten = _ten;
    this.loai = _loai;
    this.gia = _gia;
    this.khuyenMai = _khuyenMai;
    this.hinhAnh = _hinhAnh;
    this.moTa = _moTa;
    this.tinhTrang = _tinhTrang;
  }
  tinhGiaKhuyenMai = function () {
    return this.gia * (1 - this.khuyenMai);
  };
}
// export default chỉ được dùng 1 lần duy nhất trên 1 file
