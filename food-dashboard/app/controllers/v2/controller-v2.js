export let renderDanhSachMonAn = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let contentTr = /*html*/ `
     <tr>
         <td>${item.ma}</td>
         <td>${item.ten}</td>
         <td>${item.loai}</td>
         <td>${item.gia}</td>
         <td>${item.khuyenMai}</td>
         <td>0</td>
         <td>${item.tinhTrang}</td>
     </tr>
     `;

    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
export function layThongTinTuForm() {
  var maMon = document.getElementById("foodID").value;
  var tenMon = document.getElementById("tenMon").value;
  var giaMon = document.getElementById("giaMon").value;
  var loai = document.getElementById("loai").value;
  var tinhTrang = document.getElementById("tinhTrang").value;
  var khuyenMai = document.getElementById("khuyenMai").value;
  var hinhMon = document.getElementById("hinhMon").value;
  var moTa = document.getElementById("moTa").value;
  return {
    ma: maMon,
    ten: tenMon,
    gia: giaMon,
    loai,
    tinhTrang,
    khuyenMai,
    hinhAnh: hinhMon,
    moTa,
  };
}
// gia: "765.00";
// hinhAnh: "https://loremflickr.com/640/480/food";
// khuyenMai: 24296;
// loai: false;
// ma: "1";
// moTa: "Rerum ad neque numquam culpa tempore sunt cumque doloremque excepturi.";
// ten: "Nile tilapia";
// tinhTrang: false;
