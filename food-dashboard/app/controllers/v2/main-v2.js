import { layThongTinTuForm, renderDanhSachMonAn } from "./controller-v2.js";

const BASE_URL = "https://63b2c99f5901da0ab36dbaed.mockapi.io/food_es6";
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      renderDanhSachMonAn(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();

let themMonAn = () => {
  axios({
    url: BASE_URL,
    method: "POST",
    data: layThongTinTuForm(),
  })
    .then((res) => {
      fetchFoodList();
      Toastify({
        text: "Thêm món ăn thành công",
        className: "info",
        offset: {
          x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
          y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
        },
      }).showToast();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.themMonAn = themMonAn;
