import { layThongTinTuForm, showThongTinLenForm } from "./controller-v1.js";

import Food from "../../models/v1/model-v1.js";
let themMonAn = () => {
  let data = layThongTinTuForm();

  let monAn = new Food(
    data.maMon,
    data.tenMon,
    data.loai,
    data.giaMon,
    data.khuyenMai,
    data.hinhMon,
    data.moTa,
    data.tinhTrang
  );

  showThongTinLenForm(monAn);
};
window.themMonAn = themMonAn;
