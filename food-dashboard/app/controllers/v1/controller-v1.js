export function layThongTinTuForm() {
  var maMon = document.getElementById("foodID").value;
  var tenMon = document.getElementById("tenMon").value;
  var giaMon = document.getElementById("giaMon").value;
  var loai = document.getElementById("loai").value;
  var tinhTrang = document.getElementById("tinhTrang").value;
  var khuyenMai = document.getElementById("khuyenMai").value;
  var hinhMon = document.getElementById("hinhMon").value;
  var moTa = document.getElementById("moTa").value;
  return {
    maMon,
    tenMon,
    giaMon,
    loai,
    tinhTrang,
    khuyenMai,
    hinhMon,
    moTa,
  };
}

export let showThongTinLenForm = (food) => {
  document.getElementById("imgMonAn").src = food.hinhAnh;
  document.getElementById("spMa").innerText = food.ma;
  document.getElementById("spMa").innerText = food.ma;
  document.getElementById("spTenMon").innerText = food.ten;
  document.getElementById("spLoaiMon").innerText = food.loai;
  document.getElementById("spGia").innerText = food.gia;
  document.getElementById("spKM").innerText = food.khuyenMai * 10 + "%";
  document.getElementById("spGiaKM").innerText = food.tinhGiaKhuyenMai();
  document.getElementById("spTT").innerText = food.tinhTrang;
  document.getElementById("pMoTa").innerText = food.moTa;
};
