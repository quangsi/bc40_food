function Cat(_name, _age) {
  this.name = _name;
  this.age = _age;
}
let cat1 = new Cat("mun", 2);

class Dog {
  constructor(_name, _age) {
    this.age = _age;
    this.name = _name;
  }
}
let dog1 = new Dog("lulu", 2);
